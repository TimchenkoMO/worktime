#-------------------------------------------------
#
# Project created by QtCreator 2012-02-07T09:37:55
#
#-------------------------------------------------

QT       += core gui

TARGET = worktime
TEMPLATE = app

INCLUDEPATH += .\
              ../build


SOURCES += main.cpp\
        worktimedlg.cpp \
    manualworktimedlg.cpp

HEADERS  += worktimedlg.h \
    manualworktimedlg.h

FORMS    += worktimedlg.ui \
    manualworktimedlg.ui

RESOURCES += \
    icons.qrc

LIBS += -ladvapi32
