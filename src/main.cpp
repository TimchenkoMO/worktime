#include <QtGui/QApplication>
#include "worktimedlg.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("RTPTechGroup");
    QCoreApplication::setOrganizationDomain("RTPTechGroup.ru");
    QCoreApplication::setApplicationName("WorkTime");

    QApplication a(argc, argv);

    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
        return 1;
    }
    QApplication::setQuitOnLastWindowClosed(false);

    WorkTimeDlg w;
    w.show();
    
    return a.exec();
}
