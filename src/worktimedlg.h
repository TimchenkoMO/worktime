#ifndef WORKTIMEDLG_H
#define WORKTIMEDLG_H

#include "ui_worktimedlg.h"
#include <QSettings>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QMenu>
#include <QAction>

class WorkTimeDlg : public QDialog, private Ui::WorkTimeDlg
{
    Q_OBJECT    
public:
    explicit WorkTimeDlg(QWidget *parent = 0);
    void setVisible(bool visible);

protected:
    void timerEvent(QTimerEvent *event);
    void closeEvent(QCloseEvent *event);

private slots:
    void slotSetStartSystemTime();
    void slotSetManualTime();
    void slotIconActivated(QSystemTrayIcon::ActivationReason reason);
    void slotQuit();

private:
    void readSettings();
    void writeSettings();

    void setStartDateTime(QDateTime &dateTime);

    int m_timerId;

    qint64 m_workHour;
    qint64 m_workMinute;
    qint64 m_workSecond;

    QSettings m_settings;

    //Tray support
    void createTrayActions();
    void createTrayIcon();

    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
};

#endif // WORKTIMEDLG_H
