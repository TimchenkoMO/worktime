#include "manualworktimedlg.h"

ManualWorkTimeDlg::ManualWorkTimeDlg(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    dteStartWorkTime->setDateTime(QDateTime::currentDateTime());
}

QDateTime ManualWorkTimeDlg::getDateTime()
{
    return dteStartWorkTime->dateTime();
}

void ManualWorkTimeDlg::setDateTime(QDateTime &dateTime)
{
    dteStartWorkTime->setDateTime(dateTime);
}
