#include "worktimedlg.h"
#include "manualworktimedlg.h"
#include <QDebug>
#include <Windows.h>
#include <QMessageBox>
#include <QFile>
#include <QCloseEvent>

QDateTime startComputer(){
    wchar_t *logName = L"System";
    #define BUFFER_SIZE 0x10000
    HANDLE h;
    EVENTLOGRECORD *pevlr;
    BYTE bBuffer[BUFFER_SIZE];
    DWORD dwRead, dwNeeded, dwThisRecord;

    QDateTime returnDate = QDateTime::currentDateTime();
    h = OpenEventLog( NULL,logName);
    if (h == NULL)
    {
        return returnDate;
    }

    pevlr = (EVENTLOGRECORD *) &bBuffer;
    GetOldestEventLogRecord(h, &dwThisRecord);


    while (ReadEventLog(h,
                        EVENTLOG_BACKWARDS_READ |
                        EVENTLOG_SEQUENTIAL_READ,
                        0,
                        pevlr,
                        BUFFER_SIZE,
                        &dwRead,
                        &dwNeeded))
    {
        while (dwRead > 0) {
            QDateTime beginDate = QDateTime(QDate(1970,1,1));

            beginDate.setTimeSpec(Qt::UTC);
            beginDate = beginDate.toLocalTime();
            beginDate = beginDate.addSecs((unsigned long)pevlr->TimeGenerated);

            if (beginDate < QDateTime(QDate::currentDate())){
                CloseEventLog(h);
                return returnDate;
            } else if (beginDate < returnDate)
                returnDate = beginDate;

            dwRead -= pevlr->Length;
            pevlr = (EVENTLOGRECORD *) ((LPBYTE) pevlr + pevlr->Length);
        }
        pevlr = (EVENTLOGRECORD *) &bBuffer;
    }
    CloseEventLog(h);
    return returnDate;
}

WorkTimeDlg::WorkTimeDlg(QWidget *parent) :
    QDialog(parent)
  , m_settings("time.conf", QSettings::IniFormat)

{
    setupUi(this);

    m_timerId = startTimer(1000);

    m_workHour = 8;
    m_workMinute = 45;
    m_workSecond = 00;

    readSettings();

    createTrayActions();
    createTrayIcon();

    connect(btnGetStartSystemTime, SIGNAL(clicked()), this, SLOT(slotSetStartSystemTime()));
    connect(btnManualSettings, SIGNAL(clicked()),this, SLOT(slotSetManualTime()));

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(slotIconActivated(QSystemTrayIcon::ActivationReason)));

    trayIcon->show();
    setWindowIcon(QIcon(":trayicon"));
}

void WorkTimeDlg::timerEvent(QTimerEvent * event)
{
    teTimeLeft->setTime(teTimeLeft->time().addSecs(-1));
    QDialog::timerEvent(event);
}

void WorkTimeDlg::closeEvent(QCloseEvent *event)
{    
    if (trayIcon->isVisible())
    {
        hide();
        event->ignore();
    }
}

void WorkTimeDlg::readSettings()
{
    m_settings.beginGroup("State");
    setStartDateTime(m_settings.value("timeStart", QDateTime::currentDateTime()).toDateTime());
    m_settings.endGroup();

    if (teStartWorkDay->date() < QDateTime::currentDateTime().date())
    {
        setStartDateTime(QDateTime::currentDateTime());
    }
}

void WorkTimeDlg::writeSettings()
{
    //При отказе в доступе просто молчит
    if(QFile::exists(m_settings.fileName()))
    {
        QFile::Permissions isAccessFile = QFile::permissions(m_settings.fileName());
        if(!(isAccessFile & QFile::WriteUser))
        {
            QMessageBox::critical(this, this->windowTitle(), trUtf8("Ошибка записи настроек. Отказано в доступе."));
        }
    }

    m_settings.beginGroup("State");
    m_settings.setValue("timeStart", teStartWorkDay->dateTime());
    m_settings.endGroup();
}

void WorkTimeDlg::setStartDateTime(QDateTime &dateTime)
{
    teStartWorkDay->setDateTime(dateTime);

    //Время окончания
    QTime endWorkTime(dateTime.time().addSecs(31500));
    teEndWorkDay->setTime(endWorkTime);

    QTime currentTime(QDateTime::currentDateTime().time());    
    QTime allWorkTime(m_workHour, m_workMinute, m_workSecond, 0);
    QTime workTime;
    workTime = workTime.addMSecs(teStartWorkDay->time().msecsTo(currentTime));

    QTime leftTime;
    leftTime = leftTime.addMSecs(workTime.msecsTo(allWorkTime));

    //Оставшееся время
    teTimeLeft->setTime(leftTime);

    //Сохраняем изменения
    writeSettings();
}

void WorkTimeDlg::slotSetStartSystemTime()
{
   QDateTime startTimeSystem = startComputer();
   setStartDateTime(startTimeSystem);
}

void WorkTimeDlg::slotSetManualTime()
{
    ManualWorkTimeDlg manualWorkTimeDlg(this);
    manualWorkTimeDlg.setDateTime(teStartWorkDay->dateTime());

    if(manualWorkTimeDlg.exec())
    {
        setStartDateTime(manualWorkTimeDlg.getDateTime());
    }
}

void WorkTimeDlg::slotQuit()
{
    writeSettings();
    killTimer(m_timerId);
    qApp->quit();
}

void WorkTimeDlg::setVisible(bool visible)
{
    minimizeAction->setEnabled(visible);
    restoreAction->setEnabled(!visible);
    QDialog::setVisible(visible);
}

void WorkTimeDlg::slotIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
            setVisible(true);
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        ;
    }
}

void WorkTimeDlg::createTrayActions()
{
    minimizeAction = new QAction(trUtf8("С&вернуть"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    restoreAction = new QAction(trUtf8("&Востановить"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(trUtf8("&Выход"), this);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(slotQuit()));
}

void WorkTimeDlg::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":trayicon"));
    trayIcon->setContextMenu(trayIconMenu);
}

