#ifndef MANUALWORKTIMEDLG_H
#define MANUALWORKTIMEDLG_H

#include "ui_manualworktimedlg.h"

class ManualWorkTimeDlg : public QDialog, private Ui::ManualWorkTimeDlg
{
    Q_OBJECT
    
public:
    explicit ManualWorkTimeDlg(QWidget *parent = 0);
    QDateTime getDateTime();
    void setDateTime(QDateTime &dateTime);
private slots:

};

#endif // MANUALWORKTIMEDLG_H
